AddLayerWidget 1.2
==

The AddLayer Widget for ArcGIS Web AppBuilder allows users to add predefined map layers to the current map without having them defined in the web map.
