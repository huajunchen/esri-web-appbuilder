define(['dojo/_base/declare', "dojo/_base/array", "dojo/on", "dojo/_base/lang",	"dojox/widget/ColorPicker",
		'jimu/BaseWidget',
		'esri/symbols/TextSymbol', "esri/symbols/Font", "esri/Color",
		"esri/request", "esri/symbols/SimpleFillSymbol", "esri/symbols/SimpleLineSymbol",
		"esri/layers/FeatureLayer", "esri/layers/GraphicsLayer", "esri/tasks/query", "esri/graphic", "esri/geometry/Circle",	
		"dojo/parser",
	],
	function (declare, array, on, lang,ColorPicker,
		BaseWidget,
		TextSymbol, Font, Color,
		esriRequest, SimpleFillSymbol, SimpleLineSymbol,
		FeatureLayer, GraphicsLayer, Query, Graphic, Circle
		) {

		//To create a widget, you need to derive from BaseWidget.
		return declare([BaseWidget], {
			// Custom widget code goes here 

			baseClass: 'Labeling-widget',

			postCreate: function () {

				this.widgetParameter = {
					layerInfos: [],
					restLayers: [],
					layerFields: [],
					defaultColor: '#365d8d'
				};
				this.widegtResult = {
					undoId: 0,
					featureLayer: "",
					labelLayer: "",
					labelColor: ""
				}

				this.widgetParameter.restLayers = this.map.layerIds.map(function (layerId) {
					return this.map.getLayer(layerId);
				}, this).filter(function (layer) {
					return !layer.tileServers && !layer.raster && layer.arcgisProps.title != "COGMAP_Base";
				});

				this.widgetParameter.layerInfos = {};
				this.widgetParameter.restLayers.forEach(function (restLayer) {
					var newSubObj = {};
					this.widgetParameter.layerInfos[restLayer.id] = newSubObj;
					restLayer.layerInfos.filter(function (layer) {
						return !layer.subLayerIds;
					}).forEach(function (layer) {
						newSubObj[layer.id] = {
							id: layer.id,
							name: layer.name,
							url: restLayer.url + "/" + layer.id
						}
					});
				}, this);

				this.inherited(arguments);
				var labelGraphicsLayer = new GraphicsLayer();
				labelGraphicsLayer.id = "LabelGraphicsLayer";
				this.map.addLayer(labelGraphicsLayer);
				this.widegtResult.labelLayer = this.map.getLayer("LabelGraphicsLayer");

				this.colorPicker = new ColorPicker({}, this.colorPickerNode);
				this.widegtResult.labelColor = this.colorButton.style.backgroundColor = this.widgetParameter.defaultColor;

				console.log('postCreate');
			},

			startup: function () {
				this.inherited(arguments);
				console.log('startup');
			},

			onOpen: function () {
				var self = this;
				console.log('onOpen');
				this.updateVisibleLayerList();

				this.widegtResult.undoId = this.updateUndoId(self.widegtResult.undoId, 0);
				on(this.layerList, "change", lang.hitch(this, this.updateFieldList));
				on(this.colorPicker, "change", function () {
					self.widegtResult.labelColor = self.colorButton.style.backgroundColor = this.hexCode.value;
				});

				window.onclick = function (event) {
					if (!self.colorPickerPanel.contains(event.target) && event.target != self.colorButton) {
						if (self.colorPickerPanel.classList.contains('show')) {
							self.colorPickerPanel.classList.remove('show');
						}
					}
				}
			},

			onClose: function () {
				console.log('onClose');
				this._ToolOff();
				on.emit(this.layerList, "change", {
					bubbles: true,
					cancelable: true
				});
				on.emit(this.colorPicker, "change", {
					bubbles: true,
					cancelable: true
				});
			},

			resize: function () {
				console.log('resize');
				this._ToolOff();
			},
			showColorPicker: function () {
				this.colorPickerPanel.classList.toggle('show');
				this.colorPicker.setColor(this.colorButton.style.backgroundColor);
			},
			updateVisibleLayerList: function () {
				var self = this;
				this.layerList.innerHTML = "";
				this.layerList.innerHTML = this.widgetParameter.restLayers.filter((function (layer) {
					return layer.visible;
				})).map(function (layer) {
					return layer.visibleLayers.filter(function (id) {
						return id != -1;
					}).map(function (id) {
						var layerInfo = self.widgetParameter.layerInfos[layer.id][id];
						return "<option value='" + layer.id + "||" + id + "'>" + layerInfo.name + "</option>"
					}).join("");
				}).join("")
				this.updateFieldList();
			},

			updateFieldList: function () {
				var self = this;

				function displayFields(arr) {
					return arr.map(function (field) {
						return "<option value=" + field.name + ">" + field.name + "</option>"
					}).join("");
				}

				this.fieldList.innerHTML = "";
				console.log("get field list:" + this.layerList.value);

				var layerInfo = this.layerList.value.split("||");
				if (layerInfo.length < 2) {
					return;
				};
				var layer = this.widgetParameter.layerInfos[layerInfo[0]][layerInfo[1]];
				if (layer.fieldInfos && layer.fieldInfos.length > 0) {
					self.fieldList.innerHTML = displayFields(layer.fieldInfos);
				} else {
					var requestHandle = esriRequest({
						"url": layer.url,
						"content": {
							"f": "json"
						},
						"callbackParamName": "callback"
					});
					requestHandle.then(function (response) {
						layer.fieldInfos = response.fields;
						self.fieldList.innerHTML = displayFields(layer.fieldInfos);
					});
				}
			},

			_ToolOn: function () {
				var self = this;

				this.map.setInfoWindowOnClick(false);
				this.StartButton.disabled = true;
				this.StopButton.disabled = false;
				this.layerList.disabled = true;
				this.fieldList.disabled = true;

				var layerInfo = this.layerList.value.split("||");
				var layerUrl = this.widgetParameter.layerInfos[layerInfo[0]][layerInfo[1]].url

				var field = this.fieldList.value;

				var selectionSymbol = new SimpleFillSymbol(
					SimpleFillSymbol.STYLE_NULL,
					new SimpleLineSymbol("solid", new Color("yellow"), 2),
					null
				);

				self.widegtResult.featureLayer = new FeatureLayer(layerUrl, {
					mode: FeatureLayer.MODE_SELECTION,
					outFields: [field]
				});

				self.widegtResult.featureLayer.setSelectionSymbol(selectionSymbol);
				this.map.addLayers([self.widegtResult.featureLayer]);

				//when users click on the map select the feature using the map point 
				self.widegtResult.clickEvt = this.map.on("click", function (e) {


					//offset
					var offset_X = self.getDistanceForPixels(self.Offset_X.value);
					var offset_Y = self.getDistanceForPixels(self.Offset_Y.value);

					// in order to select point features, buffer map point by 10 pixels
					var radius;
					switch (self.withinDistance.lastElementChild.value) {
						case "px":
							radius = self.getDistanceForPixels(self.withinDistance.firstElementChild.value);
							radiusUnit = esri.Units.METERS;
							break;
						case "feet":
							radius = self.withinDistance.firstElementChild.value;
							radiusUnit = esri.Units.FEET;
							break;
						case "miles":
							radius = self.withinDistance.firstElementChild.value;
							radiusUnit = esri.Units.MILES;
					}

					var selectionArea = new Circle({
						center: e.mapPoint,
						radius: radius,
						radiusUnit: radiusUnit
					});

					var query = new Query();
					query.geometry = selectionArea;

					self.widegtResult.featureLayer.selectFeatures(query, FeatureLayer.SELECTION_NEW, function (selection) {
						console.log(selection.length + " features selected");
						if (selection.length > 0) {
							self.widegtResult.undoId = self.updateUndoId(self.widegtResult.undoId, 1);

							array.forEach(selection, function (item) {

								var graphicLabel = new Graphic();

								if (item.geometry.type == "polyline" || item.geometry.type == "polygon") {
									//polyline geometry can't use TextSymbol as symbol.
									graphicLabel.geometry = item.geometry.getExtent().getCenter().offset(offset_X, offset_Y);
								} else {
									graphicLabel.geometry = item.geometry.offset(offset_X, offset_Y);
								}

								var symbol = new TextSymbol().setColor(new Color(self.widegtResult.labelColor));
								symbol.font.setSize(self.fontSizePickerNode.value);

								symbol.font.setFamily("arial");
								symbol.font.setWeight(Font.WEIGHT_BOLD);
								symbol.yoffset = 4;
								symbol.text = "" + item.attributes[field] + "";

								graphicLabel.setSymbol(symbol);
								graphicLabel.undoId = self.widegtResult.undoId;
								self.widegtResult.labelLayer.add(graphicLabel);
							}, this);
						}
					});
				});
			},

			// return the distance on the map of the givne number of pixels
			getDistanceForPixels: function (pixels) {
				var distancePerPixel = 0,
					distance = 0;
				distancePerPixel = this.map.extent.getWidth() / this.map.width;
				distance = pixels * distancePerPixel;
				return distance;
			},

			updateUndoId: function (undoId, add) {
				var newUndoId = undoId + add;
				if (newUndoId == 0) {
					this.UndoButton.disabled = true;
					this.RemoveAllButton.disabled = true

				} else {
					this.UndoButton.disabled = false;
					this.RemoveAllButton.disabled = false;

				}

				return newUndoId;
			},

			_ToolOff: function () {
				if (this.widegtResult.clickEvt) {
					this.widegtResult.clickEvt.remove();
				}
				this.StartButton.disabled = false;
				this.StopButton.disabled = true;
				this.layerList.disabled = false;
				this.fieldList.disabled = false;

				this.map.removeLayer(this.widegtResult.featureLayer);

				this.map.setInfoWindowOnClick(true);
				this.widegtResult.featureLayer = "";


			},

			_RemoveLabels: function () {
				this.widegtResult.labelLayer.clear();
				this.widegtResult.undoId = this.updateUndoId(this.widegtResult.undoId, -this.widegtResult.undoId);
			},

			_UndoLabels: function () {
				var self = this;
				if (this.widegtResult.undoId > 0) {

					var item = this.widegtResult.labelLayer.graphics.filter(function (item) {
						return item.undoId == self.widegtResult.undoId;
					}).forEach(function (item) {
						self.widegtResult.labelLayer.remove(item);
					});
					this.widegtResult.undoId = this.updateUndoId(self.widegtResult.undoId, -1);
				}
			}
		});
	});